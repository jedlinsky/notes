<?php

use Illuminate\Http\Request;

$app->group(['prefix' => 'api'], function () use ($app) {
    $app->get('/', function () use ($app) {
        return '<ul><li>GET /notes</li><li>POST /notes</li><li>GET /notes/{id}</li><li>PUT /notes/{id}</li><li>DELETE /notes/{id}</li></ul>';
    });

    $app->get('notes', function () use ($app) {
        $notes = app('db')->select("SELECT completed, created_at, id, title FROM notes WHERE (deleted_at IS NULL) AND (ip = :ip)", [
            'ip' => $_SERVER['REMOTE_ADDR'],
        ]);

        foreach ($notes as $key => $note) {
            $notes[$key]->completed = $note->completed ? true : false;
            $notes[$key]->created_at = (new DateTime($note->created_at))->format('c');
        }

        return $notes;
    });

    $app->post('notes', function (Request $request) use ($app) {
        app('db')->insert("INSERT INTO notes (id, title, description, completed, ip, created_at) values (?, ?, ?, ?, ?, ?)", [
            null,
            $request->get('title'),
            $request->get('description'),
            false,
            $_SERVER['REMOTE_ADDR'],
            date('Y-m-d H:i:s'),
        ]);

        $note = getNote();

        if ($note) {
            return response()->json($note);
        }
    });

    $app->get('notes/{id:[0-9]+}', function ($id) use ($app) {
        $note = getNote($id);

        if ($note) {
            return response()->json($note);
        }
    });

    $app->put('notes/{id:[0-9]+}', function ($id, Request $request) use ($app) {
        if ($request->has('title')) {
            app('db')->update("UPDATE notes SET title = ? WHERE id = ?", [
                $request->get('title'),
                $id,
            ]);
        }

        if ($request->has('description')) {
            app('db')->update("UPDATE notes SET description = ? WHERE id = ?", [
                $request->get('description'),
                $id,
            ]);
        }

        if ($request->has('completed')) {
            app('db')->update("UPDATE notes SET completed = ? WHERE id = ?", [
                $request->get('completed'),
                $id,
            ]);
        }

        $note = getNote($id);

        if ($note) {
            return response()->json($note);
        }
    });

    $app->delete('notes/{id:[0-9]+}', function ($id) use ($app) {
        app('db')->update("UPDATE notes SET deleted_at = ? WHERE id = ?", [
            date('Y-m-d H:i:s'),
            $id,
        ]);
    });
});

function getNote($id = false)
{
    if (!$id) {
        $id = app('db')->connection()->getPdo()->lastInsertId();
    }

    $note = app('db')->select("SELECT id, title, description, completed, created_at FROM notes WHERE (deleted_at IS NULL) AND (ip = :ip) AND (id = :id)", [
        'ip' => $_SERVER['REMOTE_ADDR'],
        'id' => $id,
    ]);

    if ($note) {
        $note = $note[0];

        $note->completed = $note->completed ? true : false;
        $note->created_at = (new DateTime($note->created_at))->format('c');

        return $note;
    }

    return false;
}
