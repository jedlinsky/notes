# Notes Demo App

([demo](http://notes.visecta.cz/))

***

Demo application for the show of a simple CRUD interaction between Node.js server and Angular 2.

Added Lumen as fallback for PHP API (demo app is hosted on Wedos which doesn't support Node.js).

***

## Description

* Node.js server with Express.js.
* One very simple E2E test.
* MongoDB.
* Custom form validation settings in .angular-cli.json.
* Delay server-side middleware for demo purposes (spinner).
* Built with Angular CLI.
* Translations in i18n directory, automatic language detection by browser settings, fallback to en
* Custom responsive styles.
* Developed and tested @ Windows 10, Chrome 56

***

## Prerequirements

* [Node.js: ^7.6.0](https://nodejs.org/)
* [Angular CLI](https://github.com/angular/angular-cli)
* [MongoDB: ^3.4.0](https://www.mongodb.com/)

***

## Dependencies Installation

* $ npm install

***

## Development Server

* start MongoDB ([mongod](http://stackoverflow.com/questions/20796714/how-do-i-start-mongo-db-from-windows))
* $ npm run nodemon
* $ ng serve
* navigate to http://localhost:9000/

***

## Build

* $ ng build -prod

***

## E2E Test

* $ ng e2e

***

Vojtěch Jedlinský © 2017
