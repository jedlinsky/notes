import { browser, element, by } from 'protractor';

export class NotesPage {
  navigateTo() {
    return browser.get('/');
  }

  getTitle() {
    return browser.getTitle();
  }
}
