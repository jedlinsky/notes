import { NotesPage } from './app.po';

describe('notes app', () => {
  let page: NotesPage;

  beforeEach(() => {
    page = new NotesPage();
  });

  it('should have a title', function() {
    page.navigateTo();

    expect(page.getTitle()).toEqual('Notes');
  });
});
