const config = require('../../.angular-cli.json');

export const environment = {
  apiUrl: 'http://notes-api.visecta.cz/api',
  production: true,
};
