import { Pipe, PipeTransform } from '@angular/core';

import { TranslateService } from 'ng2-translate';

const fecha = require('fecha/fecha.js');

@Pipe({
  name: 'formatDate',
  pure: false,
})
export class FormatDatePipe implements PipeTransform {
  formated: string;
  lang: string;

  constructor(private translate: TranslateService) {
  }

  transform(value: any, args?: any): any {
    if (this.lang === this.translate.currentLang) {
      return this.formated;
    }

    if (!value) {
      return undefined;
    }

    this.lang = this.translate.currentLang;

    const en = 'MM/DD/YYYY – hh:mm:ss a';
    const cs = 'DD. MM. YYYY – HH:mm:ss';

    this.formated = fecha.format(new Date(value), this.lang === 'cs' ? cs : en);

    return this.formated;
  }
}
