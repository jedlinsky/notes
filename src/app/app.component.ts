import { Component } from '@angular/core';

import { TranslateService } from 'ng2-translate';

import { NoteService } from './note.service';

@Component({
  selector: 'app-root',
  styleUrls: ['./app.component.scss'],
  templateUrl: './app.component.html',
})
export class AppComponent {
  constructor(private translate: TranslateService, public noteService: NoteService) {
    const defaultLang = 'en';

    translate.addLangs([
      'en',
      'cs',
    ]);

    let lang = localStorage.getItem('lang');
    lang = translate.getLangs().find(l => l === lang || l === translate.getBrowserLang());

    translate.setDefaultLang(defaultLang);

    this.translate.use(lang ? lang : defaultLang);
  }
}
