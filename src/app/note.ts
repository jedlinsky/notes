import { Observable } from 'rxjs/Observable';

export class Note {
  static sort(notes: Observable<Note[]>): Observable<Note[]> {
    return notes.map(note => note.sort((a, b) => new Date(b.created_at).getTime() - new Date(a.created_at).getTime()));
  }

  constructor(
    public id: number,
    public title: string,
    public completed: boolean = false,
    public created_at: any,
    public description?: string
  ) {
  }
}
