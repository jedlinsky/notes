import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';

import { TranslateModule } from 'ng2-translate';

import { appRoutes } from './app.routes';

import { AppComponent } from './app.component';
import { FormatDatePipe } from './format-date.pipe';
import { NavComponent } from './nav/nav.component';
import { NoteComponent } from './note/note.component';
import { NoteCreateComponent } from './note-create/note-create.component';
import { NoteListComponent } from './note-list/note-list.component';
import { NoteListItemComponent } from './note-list-item/note-list-item.component';
import { NoteService } from './note.service';

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    NoteComponent,
    NoteCreateComponent,
    NoteListComponent,
    NoteListItemComponent,

    FormatDatePipe,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,

    TranslateModule.forRoot(),

    appRoutes,
  ],
  providers: [
    NoteService,
  ],
  bootstrap: [
    AppComponent,
  ],
})
export class AppModule {
}
