import { Component } from '@angular/core';

import { TranslateService } from 'ng2-translate';

@Component({
  selector: 'app-nav',
  styleUrls: ['./nav.component.scss'],
  templateUrl: './nav.component.html',
})
export class NavComponent {
  constructor(public translate: TranslateService) {
  }

  set(lang: string): void {
    this.translate.use(lang);

    localStorage.setItem('lang', lang);
  }
}
