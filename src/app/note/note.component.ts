import {
  animate,
  Component,
  HostBinding,
  OnDestroy,
  OnInit,
  state,
  style,
  transition,
  trigger
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import { TranslateService } from 'ng2-translate';

import { Note } from '../note';
import { NoteService } from '../note.service';

@Component({
  animations: [
    trigger('routerTransition', [
      transition(':enter', [
        style({transform: 'translateX(100%)'}),

        animate('1s cubic-bezier(.075, .82, .165, 1)', style({transform: 'translateX(0)'}))
      ]),

      transition(':leave', [
        style({transform: 'translateX(0)'}),

        animate('1s cubic-bezier(.075, .82, .165, 1)', style({transform: 'translateX(100%)'}))
      ])
    ]),
  ],
  selector: 'app-note',
  styleUrls: ['./note.component.scss'],
  templateUrl: './note.component.html',
})
export class NoteComponent implements OnDestroy, OnInit {
  @HostBinding('@routerTransition') tranistion = true;

  note: Observable<Note>;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    public noteService: NoteService
  ) {
  }

  ngOnInit(): void {
    this.note = this.noteService.note;

    this.route.params.subscribe(params => {
      const id = +params['id'];

      if (isNaN(id)) {
        this.router.navigate(['/']);

        return;
      }

      this.noteService.getNote(id);
    });

    this.note.subscribe(note => this.noteService.setTitle(note.title));

    setTimeout(() => this.noteService.createTitleElem.nativeElement.blur());

    document.body.classList.add('overflow');
  }

  ngOnDestroy(): void {
    this.noteService.cleanNote();

    this.noteService.setTitle();

    this.noteService.createTitleElem.nativeElement.focus();

    document.body.classList.remove('overflow');
  }
}
