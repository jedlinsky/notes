import { Routes, RouterModule } from '@angular/router';

import { NoteComponent } from './note/note.component';

const routes: Routes = [
  {
    component: NoteComponent,
    path: ':id',
  }
];

export const appRoutes = RouterModule.forRoot(routes);
