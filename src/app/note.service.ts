import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { ElementRef, Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import { Title } from '@angular/platform-browser';

import 'rxjs/add/operator/filter';
import 'rxjs/add/operator/first';
import 'rxjs/add/operator/map';

import { TranslateService } from 'ng2-translate';

import swal from 'sweetalert2';

import { environment } from '../environments/environment';

import { Note } from './note';

const apiUrl = `${environment.apiUrl}/notes`;
const spinner = document.querySelector('.spinner');

@Injectable()
export class NoteService {
  private _note: BehaviorSubject<Note> = new BehaviorSubject<Note>(Note.prototype);
  private _notes: BehaviorSubject<Note[]> = new BehaviorSubject<Note[]>([]);
  private _notesAll: BehaviorSubject<Note[]> = new BehaviorSubject<Note[]>([]);
  private _notesCompleted: BehaviorSubject<Note[]> = new BehaviorSubject<Note[]>([]);

  private spinnerTimeout;

  note: Observable<Note> = this._note.asObservable();
  notes: Observable<Note[]> = this._notes.asObservable();
  notesAll: Observable<Note[]> = this._notesAll.asObservable();
  notesCompleted: Observable<Note[]> = this._notesCompleted.asObservable();

  defaultSiteTitle: string = this.titleService.getTitle();

  detail = false;
  detailId: number;
  edit = false;
  willEdit = false;

  createTitleElem: ElementRef;

  constructor(
    private http: Http,
    private router: Router,
    private titleService: Title,
    private translate: TranslateService
  ) {
    this.getNotes();
  }

  getNotes(): void {
    this.http.get(apiUrl)
      .map(res => res.json())
      .subscribe(notes => {
        this._notesAll.subscribe(ns => this.categorize(ns));

        this._notesAll.next(notes);

        this.initSpinner();
      });
  }

  categorize(notes: Note[]): void {
    this._notes.next(notes.filter(note => note.completed === false));
    this._notesCompleted.next(notes.filter(note => note.completed));
  }

  getNote(id: number): void {
    this.edit = false;

    this.showSpinner();

    this.http.get(`${apiUrl}/${id}`)
      .map(res => res.json())
      .subscribe(
        note => {
          this.detailId = note.id;

          this._note.next(note);

          if (this.willEdit) {
            this.edit = true;

            this.willEdit = false;
          }
        },
        () => {
          this.router.navigate(['/']);

          this.hideSpinner();
        },
        () => this.hideSpinner()
      );
  }

  addNote(data): void {
    this.showSpinner();

    this.http.post(apiUrl, data)
      .map(res => res.json())
      .subscribe(
        note => {
          this.notesAll.first().subscribe(notes => {
            notes.push(note);

            this._notesAll.next(notes);

            this.router.navigate(['/', note.id]);
          });
        },
        err => console.log(err),
        () => this.hideSpinner()
      );
  }

  changeState(completed: boolean, id?: number): void {
    this.showSpinner();

    this.isDetail(id);

    if (!id) {
      id = this.detailId;
    }

    this.http.put(`${apiUrl}/${id}`, {completed: completed})
      .map(res => res.json())
      .subscribe(
        note => {
          this.notesAll.first().subscribe(notes => {
            notes.forEach((n, i) => {
              if (n.id === note.id) {
                notes[i] = note;
              }
            });

            this._notesAll.next(notes);

            // if is note selected, update detail data
            if (this.detail) {
              this._note.next(note);
            }

            // if is note selected and mark as completed, redirect to root
            if (completed && this.detail) {
              this.router.navigate(['/']);
            }
          });
        },
        err => console.log(err),
        () => this.hideSpinner()
      );
  }

  editNoteToggle(id?: number): void {
    this.isDetail(id);

    if (!id) {
      id = this.detailId;

      this.edit = !this.edit;
    } else {
      if (this.detailId === id) {
        this.edit = !this.edit;
      } else {
        this.willEdit = true;

        this.router.navigate(['/', id]);
      }
    }
  }

  editNote(values: Object): void {
    this.showSpinner();

    this.http.put(`${apiUrl}/${this.detailId}`, values)
      .map(res => res.json())
      .subscribe(
        note => {
          this.notesAll.first().subscribe(notes => {
            notes.forEach((n, i) => {
              if (n.id === note.id) {
                notes[i] = note;
              }
            });

            this._note.next(note);
            this._notesAll.next(notes);

            this.edit = false;
          });
        },
        err => console.log(err),
        () => this.hideSpinner()
      );
  }

  removeNote(id?: number): void {
    this.isDetail(id);

    if (!id) {
      id = this.detailId;
    }

    this.translate.getTranslation(this.translate.currentLang).subscribe(lang => {
      swal({
        cancelButtonText: lang.removeCancel,
        confirmButtonColor: '#d20000',
        confirmButtonText: lang.removeConfirm,
        preConfirm: () => new Promise(resolve => {
          this.showSpinner();

          this.http.delete(`${apiUrl}/${id}`).subscribe(
            () => {
              this.notesAll.first().subscribe(notes => {
                notes = notes.filter(note => note.id !== id);

                this._notesAll.next(notes);

                if (this.detail) {
                  this.router.navigate(['/']);
                }

                resolve();
              });
            },
            err => console.log(err),
            () => this.hideSpinner()
          );
        }),
        showCancelButton: true,
        text: lang.removeText,
        title: lang.removeTitle,
        type: 'warning',
      }).then(() => {
        swal({
          text: lang.removeSuccessText,
          timer: 1500,
          title: lang.removeSuccessTitle,
          type: 'success',
        }).catch(swal.noop);

        setTimeout(() => this.createTitleElem.nativeElement.focus());
      }).catch(swal.noop);
    });
  }

  isDetail(id?: number): void {
    this.note.first().subscribe(note => {
      if (!id || id === note.id) {
        this.detail = true;
      }
    });
  }

  cleanNote(): void {
    this.edit = false;
    this.detail = false;
    this.detailId = undefined;

    this._note.next(Note.prototype);
  }

  setTitle(title?: string): void {
    this.titleService.setTitle(title ? `${title} – ${this.defaultSiteTitle}` : this.defaultSiteTitle);
  }

  initSpinner(): void {
    spinner.classList.add('hidden', 'transparent');
  }

  hideSpinner(): void {
    clearTimeout(this.spinnerTimeout);

    this.spinnerTimeout = setTimeout(() => spinner.classList.add('hidden'), 200);
  }

  showSpinner(): void {
    clearTimeout(this.spinnerTimeout);

    this.spinnerTimeout = setTimeout(() => spinner.classList.remove('hidden'), 100);
  }
}
