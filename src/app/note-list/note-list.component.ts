import { Component } from '@angular/core';
import { Note } from '../note';
import { NoteService } from '../note.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-note-list',
  styleUrls: ['./note-list.component.scss'],
  templateUrl: './note-list.component.html',
})
export class NoteListComponent {
  notes: Observable<Note[]>;
  notesCompleted: Observable<Note[]>;

  constructor(private noteService: NoteService) {
    this.notes = Note.sort(noteService.notes);
    this.notesCompleted = Note.sort(noteService.notesCompleted);
  }
}
