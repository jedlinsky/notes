import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { NoteService } from '../note.service';

const config = require('../../../.angular-cli.json');

@Component({
  selector: 'app-note-create',
  styleUrls: ['./note-create.component.scss'],
  templateUrl: './note-create.component.html',
})
export class NoteCreateComponent implements OnInit {
  @Input() edit = false;

  @ViewChild('description') description: ElementRef;
  @ViewChild('title') title: ElementRef;

  formValidation: any = config.formValidation;

  form: FormGroup;
  chars: number = this.formValidation.descriptionMaxLength;

  constructor(private fb: FormBuilder, private noteService: NoteService) {
    this.form = fb.group({
      description: [
        null,
        Validators.maxLength(this.formValidation.descriptionMaxLength),
      ],

      title: [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(this.formValidation.titleMinLength),
          Validators.maxLength(this.formValidation.titleMaxLength),
        ]),
      ],
    });
  }

  ngOnInit(): void {
    this.noteService.createTitleElem = this.title;

    if (!this.edit) {
      return;
    }

    this.noteService.note.first().subscribe(note => {
      this.form.setValue({
        description: note.description,
        title: note.title,
      });

      this.title.nativeElement.focus();

      setTimeout(() => this.checkLength(this.description.nativeElement.value));
    });
  }

  addNote(): void {
    if (this.edit) {
      this.editNote();

      return;
    }

    this.noteService.addNote(this.form.value);

    if (this.form.valid) {
      this.title.nativeElement.focus();

      this.form.reset();

      this.form.patchValue({
        title: '',
      });

      setTimeout(() => this.checkLength(this.description.nativeElement.value));
    }
  }

  editNote(): void {
    this.noteService.editNote(this.form.value);
  }

  checkLength(value: string): void {
    this.chars = this.formValidation.descriptionMaxLength - value.length;
  }
}
