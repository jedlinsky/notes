import express from 'express';
import bodyParser from 'body-parser';
import http from 'http';
import path from 'path';

import api from './server/routes/api';
import config from './.angular-cli';

const app = express();
const port = config.defaults.serve.port;

app.use((req, res, next) => {
  let url = `http://localhost:${port}`;

  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.setHeader('Access-Control-Allow-Origin', url);

  next();
});

// delay, just for fun
app.use((req, res, next) => setTimeout(next, 1000));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(express.static(path.join(__dirname, 'dist')));
app.use('/api', api);

app.set('port', port + 1);

http.createServer(app).listen(app.settings.port, () => console.log(`API running on localhost:${port + 1}`));
