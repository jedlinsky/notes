import mongoose from 'mongoose';
import autoIncrement from 'mongoose-auto-increment';

const config = require('../../.angular-cli.json').formValidation;

const connection = mongoose.connect('mongodb://localhost/notes');
autoIncrement.initialize(connection);

mongoose.Promise = global.Promise;

const Schema = mongoose.Schema;

let noteSchema = new Schema({
  completed: {
    default: false,
    type: Boolean,
  },
  created_at: {
    default: Date.now,
    type: Date,
  },
  deleted_at: {
    default: null,
    type: Date,
  },
  description: {
    maxlength: config.descriptionMaxLength,
    type: String,
  },
  title: {
    maxlength: config.titleMaxLength,
    minlength: config.titleMinLength,
    required: true,
    type: String,
  },
});

noteSchema.plugin(autoIncrement.plugin, {
  field: 'id',
  model: 'Note',
  startAt: 1,
});

module.exports = connection.model('Note', noteSchema);
