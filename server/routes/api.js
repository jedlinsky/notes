import express from 'express';

import Note from '../database/note';

const router = express.Router();

router.get('/', (req, res) => {
  res.send(`
    <ul>
      <li>GET /notes</li>
      <li>POST /notes</li>
      <li>GET /notes/{id}</li>
      <li>PUT /notes/{id}</li>
      <li>DELETE /notes/{id}</li>
    </ul>
  `);
});

router.get('/notes', (req, res) => {
  Note.find({deleted_at: null}, [
    'completed',
    'created_at',
    'id',
    'title',
  ], (err, notes) => {
    if (err) {
      throw err;
    }

    res.send(notes);
  });
});

router.post('/notes', (req, res) => {
  let note = new Note(req.body);
  let result

  note.save().then(data => {
    res.send({
      completed: data.completed,
      created_at: data.created_at,
      description: data.description,
      id: data.id,
      title: data.title,
    });
  });
});

router.get('/notes/:id(\\d+)/', (req, res) => {
  Note.findOne({id: req.params.id, deleted_at: null}, (err, note) => {
    if (err) {
      throw err;
    }

    res.send(note);
  });
});

router.put('/notes/:id(\\d+)/', (req, res) => {
  Note.findOneAndUpdate({id: req.params.id}, req.body, {new: true}, (err, note) => {
    if (err) {
      throw err;
    }

    res.send(note);
  });
});

router.delete('/notes/:id(\\d+)/', (req, res) => {
  Note.findOneAndUpdate({id: req.params.id}, {'deleted_at': Date.now()}, err => {
    if (err) {
      throw err;
    }

    res.send();
  });
});

module.exports = router;
